-----------------------------------------------------------------------------------------
--
-- globals.lua
--
-----------------------------------------------------------------------------------------
--[[
	
Global Variables for my development use

this module needs to be included and referenced by each module that needs it as such. 

					eg
					local globals = require( "globals" )
Now you can use the myData table just like you would _G and circumvent the risk of overwriting things in the _G table, as well as avoid the general issues and pitfalls that come along with globals.

		globals.myVariable = 10
--]]

--my global space
local M = {}

return M



