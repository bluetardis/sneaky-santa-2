-------------------------------------------------------------------------
--Blue Tardis
--Sneaky Santa
--Created by Peter Bishop & Nickolas Kola
--
-- MAIN.LUA
-------------------------------------------------------------------------


--Hide status bar from the beginning
display.setStatusBar( display.HiddenStatusBar ) 

--==================================================================================================
-- pause on start so you can see splashscreen
--==================================================================================================

-- local startTime = os.time()
-- local endTime
-- for i = 1, 6000 do
-- 	print ("Calculating Timers...",i);
-- 	endTime = os.time()
-- 	endTime = system.getTimer()
-- 	i=i+1
-- end
--print ("endTime",endTime); 

--==================================================================================================
	
	

	
--==================================================================================================
-- Initial Settings
--==================================================================================================

--globals for us developers to work with 
globals = require("globals")


--initialise the ads plugin
globals.ads = require "ads"

--initialise the flurry plugin
--globals.flurry = require "analytics"

--are we on the simulator or a device
globals.isSimulator = "simulator" == system.getInfo( "environment" )
--print ("simulator is",globals.isSimulator)


-- Import sqlite and storyboard
local sqlite3 = require("sqlite3")
local storyboard = require "storyboard"
storyboard.purgeOnSceneChange = true --So it automatically purges for us.


--seed the random number generator
math.randomseed(os.time())
math.random()
math.random()


--setup sound.
globals.sounds = true
globals.music = true


--Create a constantly looping background sound...
globals.bgSound = audio.loadStream("sounds/bgSound.mp3")
audio.reserveChannels(11)   --Reserve its channel
audio.play(globals.bgSound, {channel=10, loops=-1}) --Start looping the sound.

--Activate multi-touch so we can press multiple buttons at once.
system.activate("multitouch")


--==================================================================================================
-- Game Info and Levels
--==================================================================================================

--death counter loop (when do we show video)
globals.deathcount = 1

--Create some level globals used in the game itself/gameWon/gameOver scenes.
_G.amountofLevels = 7
_G.currentLevel = 1
_G.levelScore = 0  

--Create a database table for holding the high scores per level in.
--We only need a small database as we dont need to save much information.
local dbPath = system.pathForFile("levelScores.db3", system.DocumentsDirectory)
local db = sqlite3.open( dbPath )	

--Current 7 levels. Add more rows to make more levels available. Also remember if
--you add an extra row into this database you need to add an exta level to
--"amountofLevels" above.
local tablesetup = [[ 
		CREATE TABLE scores (id INTEGER PRIMARY KEY, highscore); 
		INSERT INTO scores VALUES (NULL, '0'); 
		INSERT INTO scores VALUES (NULL, '0'); 
		INSERT INTO scores VALUES (NULL, '0'); 
		INSERT INTO scores VALUES (NULL, '0');
		INSERT INTO scores VALUES (NULL, '0');
		INSERT INTO scores VALUES (NULL, '0');
		INSERT INTO scores VALUES (NULL, '0');
	]]
db:exec( tablesetup ) --Create it now.
db:close() --Then close the database





--==================================================================================================
-- Show video ad function and event listener
--==================================================================================================

 function vungleListener( event )
 
    -- Video ad not yet downloaded and available
   if ( event.type == "adStart" and event.isError ) then
		--print ("Video not ready, asking for Admob")
         ads:setCurrentProvider( "admob" )
         ads.show( "interstitial" )
		
   elseif ( event.type == "adEnd" ) then
      -- Ad was successfully shown and ended; hide the overlay so the app can resume.
      storyboard.hideOverlay()
		--print ("Ad ended successfully")
   else
      print( "Received event", event.type )
   end
   return true
end

 function adMobListener( event )
   if ( event.isError ) then
		print ("adMobListener  - error")   
		--storyboard.showOverlay( "selfpromo" ) -- could be next step for cross promotion...
   end
   return true
end


--==================================================================================================
-- Setup Variables for  Banner Ads
--==================================================================================================
	
	--parameter table for ad positioning
	globals.myAdy = display.contentHeight - 50 --setting position at bottom of screen
	globals.params = {
		--isBackButtonEnabled = true,
		x = 0,
		y = globals.myAdy,		
		interval=30,
	}

--==================================================================================================
-- If we have internet then initialise our ads and flurry if needed **ref to above internet test
--==================================================================================================

	if ( system.getInfo("platformName") == "Android" ) then
		print ("Platform: Android")
			
		--flurry
		--local flurry_key = "123456789" --android
		--globals.flurry.init (flurry_key)
		--globals.flurry.logEvent("Launched")
		
		globals.ads.init( "vungle", "com.bluetardis.sneakysanta.android", vungleListener )
		globals.ads.init( "admob", "ca-app-pub-3396843773101306/6579098078", adMobListener )
		globals.ads:setCurrentProvider( "vungle" )
		--globals.ads.show( "banner", globals.params )

	else -- its iOS
		print ("Platform: iOS")
		
		--flurry
		--local flurry_key = "0987654321" -- ios
		--globals.flurry.init (flurry_key)
		--globals.flurry.logEvent("Launched")
		
		globals.ads.init( "vungle", "com.bluetardis.sneakysanta.ios", vungleListener )
		--globals.ads.init( "iads", "com.bluetardis.mayday", iAdsListener )
		globals.ads.init( "admob", "ca-app-pub-3396843773101306/2009297671", adMobListener )
		
		globals.ads:setCurrentProvider( "vungle" )
		--globals.ads.show( "banner", globals.params )
	end



--==================================================================================================
--Now change scene to go to the menu.
--==================================================================================================
storyboard.gotoScene( "menu", "fade", 400 )

