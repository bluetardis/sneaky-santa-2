-------------------------------------------------------------------------
--BlueTardis Apps
--Sneaky Santa
--Created by Peter Bishop & Nickolas Kola
-------------------------------------------------------------------------


--Localise the module. Elimates the need for module package seeall technique.
local M = {}


--This array holds each different screens information. Each screen is 480 pixels wide!
--The movement function in game.lua calls creates each of these screens in turn.
--E.g. blocks/special blocks/obstacles/stars positions...
--You can easily add your own by copying and pasting mine to make the game more interesting!
M = {
	------------------------------------------
	--Screen 1
	------------------------------------------
	{
		--There are 3 different types of block. Select which one you want per block.
		--"breakable" - Are blocks that you can smash
		--"special" - Is a block that a coin will come out of once.
		--"pushable" - A special block you can push around.
		--"plain" - Is a normal block that does nothing.
		blocks = {
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={0,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={50,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={100,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={150,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={200,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={250,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={300,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={350,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={400,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={450,276},
			},
						{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={500,276},
			},
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
			{	
				widthHeight = {30,42},
				position = {0,226},
			},
			{	
				widthHeight = {30,42},
				position = {100,226},
			},
			{	
				widthHeight = {30,42},
				position = {200,226},
			},
			{	
				widthHeight = {30,42},
				position = {300,226},
			},
			{	
				widthHeight = {30,42},
				position = {400,226},
			},
			{	
				widthHeight = {30,42},
				position = {500,226},
			},
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {
			{	
				widthHeight = {45,16},
				position = {50,226},
			},
			{	
				widthHeight = {45,16},
				position = {150,226},
			},
			{	
				widthHeight = {45,16},
				position = {250,226},
			},	
			{	
				widthHeight = {45,16},
				position = {350,226},
			},	
			{	
				widthHeight = {45,16},
				position = {450,226},
			},
			
		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
			{
				widthHeight = {50,48},
				position ={300,226},
				allowedMovement = 290, --How far left-right the enemy can walk.
				speed = -22 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,226},
				allowedMovement = 290, --How far left-right the enemy can walk.
				speed = -18 --How fast they walk. Start walking left.
			},
						{
				widthHeight = {50,48},
				position ={480,226},
				allowedMovement = 290, --How far left-right the enemy can walk.
				speed = -18 --How fast they walk. Start walking left.
			},
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		flags = {
		},
		--You can add more categories here. Just remember to add them into the
		--creationSections function in the game.lua file.
	},

	-------------------------------------------
	--Screen 2
	-------------------------------------------
	{
		--Create the blocks for this screen. Leave blank for none.
		blocks = {
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={66.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={116.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={166.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={216.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={266.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={316.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={366.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={416.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={466.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={516.5,276},
			},
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
			{	
				widthHeight = {30,42},
				position = {116.5,226},
			},
			{	
				widthHeight = {30,42},
				position = {216.5,226},
			},
			{	
				widthHeight = {30,42},
				position = {316.5,226},
			},
			{	
				widthHeight = {30,42},
				position = {416.5,226},
			},
			{	
				widthHeight = {30,42},
				position = {516.5,226},
			},
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {

			{	
				widthHeight = {50,16},
				position = {66.5,226},
			},	
			{	
				widthHeight = {50,16},
				position = {166.5,226},
			},
			{	
				widthHeight = {50,16},
				position = {266.5,226},
			},
								{	
				widthHeight = {50,16},
				position = {366.5,226},
			},	
			{	
				widthHeight = {50,16},
				position = {466.5,226},
			},
		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
			{
				widthHeight = {50,48},
				position ={0,226},
				allowedMovement = 300, --How far left-right the enemy can walk.
				speed = -22 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -20 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -16 --How fast they walk. Start walking left.
			},
						{
				widthHeight = {50,48},
				position ={480,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -16 --How fast they walk. Start walking left.
			},
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		flags = {
		},
	},

	-------------------------------------------
	--Screen 3
	-------------------------------------------
	{
		--Create the blocks for this screen. Leave blank for none.
		blocks = {
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={83.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={133.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={183.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={233.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={283.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={333.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={383.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={433.5,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={483.5,276},
			},
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
					{	
				widthHeight = {30,42},
				position = {133.5,226},
			},
			{	
				widthHeight = {30,42},
				position = {233.5,226},
			},
			{	
				widthHeight = {30,42},
				position = {333.5,226},
			},
			{	
				widthHeight = {30,42},
				position = {433.5,226},
			},
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {
							{	
				widthHeight = {50,16},
				position = {83.5,226},
			},	
			{	
				widthHeight = {50,16},
				position = {183.5,226},
			},
			{	
				widthHeight = {50,16},
				position = {283.5,226},
			},
			{	
				widthHeight = {50,16},
				position = {383.5,226},
			},	
			{	
				widthHeight = {50,16},
				position = {483.5,226},
			},
		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
					{
				widthHeight = {50,48},
				position ={0,226},
				allowedMovement = 300, --How far left-right the enemy can walk.
				speed = -22 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -20 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -16 --How fast they walk. Start walking left.
			},
						{
				widthHeight = {50,48},
				position ={480,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -16 --How fast they walk. Start walking left.
			},
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		flags = {
		},
	},


	-------------------------------------------
	--Screen 4
	-------------------------------------------
	{
		--Create the blocks for this screen. Leave blank for none.
		blocks = {
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={50,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={100,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={150,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={200,276},
			},
						{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={250,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={300,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={350,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={400,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={450,276},
			},
						{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={500,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={550,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={600,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={650,276},
			},
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
							{	
				widthHeight = {30,42},
				position = {50,226},
			},
			{	
				widthHeight = {30,42},
				position = {150,226},
			},
			{	
				widthHeight = {30,42},
				position = {250,226},
			},
			{	
				widthHeight = {30,42},
				position = {350,226},
			},
			{	
				widthHeight = {30,42},
				position = {450,226},
			},
						{	
				widthHeight = {30,42},
				position = {550,226},
			},
			{	
				widthHeight = {30,42},
				position = {650,226},
			},
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {
						{	
				widthHeight = {50,16},
				position = {100,226},
			},	
			{	
				widthHeight = {50,16},
				position = {200,226},
			},
			{	
				widthHeight = {50,16},
				position = {300,226},
			},
			{	
				widthHeight = {50,16},
				position = {400,226},
			},	
			{	
				widthHeight = {50,16},
				position = {500,226},
			},

		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
			{
				widthHeight = {50,48},
				position ={650,226},
				allowedMovement = 90, --How far left-right the enemy can walk.
				speed = -22 --How fast they walk. Start walking left.
			},
						{
				widthHeight = {50,48},
				position ={650,226},
				allowedMovement = 90, --How far left-right the enemy can walk.
				speed = -18 --How fast they walk. Start walking left.
			},
						{
				widthHeight = {50,48},
				position ={650,226},
				allowedMovement = 90, --How far left-right the enemy can walk.
				speed = -18 --How fast they walk. Start walking left.
			},
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		flags = {
		},
	},

	-------------------------------------------
	--Screen 5
	--This is going to be the last screen for this
	--level, so all i do is make a tree! (end point)
	-------------------------------------------
	{
		--Create the blocks for this screen. Leave blank for none.
		blocks = {
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {
		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
					{
				widthHeight = {50,48},
				position ={0,226},
				allowedMovement = 300, --How far left-right the enemy can walk.
				speed = -22 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -20 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -16 --How fast they walk. Start walking left.
			},
						{
				widthHeight = {50,48},
				position ={480,226},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -35 --How fast they walk. Start walking left.
			},
								{
				widthHeight = {50,48},
				position ={480,226},
				allowedMovement = 480, --How far left-right the enemy can walk.
				speed = -45 --How fast they walk. Start walking left.
			},
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		--To edit its appearance and functions you would have to edit the createSection function in the game.lua file.
		flags = {
			{	
				widthHeight = {80,140},
				position = {320,276}, --BottomCenterReferencePoint
			},	
		},
	},
}


--Set up the screen bounds..
--Limits you going too far left or right. The first one should ALWAYS be 0.
--The second value controls how far right in pixels you can travel. 
--480 = 1 screen, 960 = 2 screens, 1440 = 3 screens, 1920 = 4 screens, 2400 = 5 screens.
M.screenBounds = {0,2400}




--Return it all to the game.
return M

