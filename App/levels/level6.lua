-------------------------------------------------------------------------
--BlueTardis Apps
--Sneaky Santa
--Created by Peter Bishop & Nickolas Kola
-------------------------------------------------------------------------


--Localise the module. Elimates the need for module package seeall technique.
local M = {}


--This array holds each different screens information. Each screen is 480 pixels wide!
--The movement function in game.lua calls creates each of these screens in turn.
--E.g. blocks/special blocks/obstacles/stars positions...
--You can easily add your own by copying and pasting mine to make the game more interesting!
M = {
	------------------------------------------
	--Screen 1
	------------------------------------------
	{
		--There are 3 different types of block. Select which one you want per block.
		--"breakable" - Are blocks that you can smash
		--"special" - Is a block that a coin will come out of once.
		--"pushable" - A special block you can push around.
		--"plain" - Is a normal block that does nothing.
		blocks = {
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={0,276},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={50,126},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={100,126},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={150,126},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={200,126},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={250,126},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={300,276},
			},
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
			{	
				widthHeight = {30,42},
				position = {0,216},
			},
			{	
				widthHeight = {30,42},
				position = {300,216},
			},
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {
			{	
				widthHeight = {45,16},
				position = {50,76},
			},
			{	
				widthHeight = {45,16},
				position = {100,76},
			},
			{	
				widthHeight = {45,16},
				position = {150,76},
			},	
			{	
				widthHeight = {45,16},
				position = {200,76},
			},	
			{	
				widthHeight = {45,16},
				position = {250,76},
			},
			
		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
			{
				widthHeight = {50,48},
				position ={300,276},
				allowedMovement = 250, --How far left-right the enemy can walk.
				speed = -16 --How fast they walk. Start walking left.
			},
						{
				widthHeight = {50,48},
				position ={300,276},
				allowedMovement = 250, --How far left-right the enemy can walk.
				speed = -12 --How fast they walk. Start walking left.
			},
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		flags = {
		},
		--You can add more categories here. Just remember to add them into the
		--creationSections function in the game.lua file.
	},

	-------------------------------------------
	--Screen 2
	-------------------------------------------
	{
		--Create the blocks for this screen. Leave blank for none.
		blocks = {
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={150,220},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={300,170},
			},
			
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
			{	
				widthHeight = {30,42},
				position = {150,160},
			},
			{	
				widthHeight = {30,42},
				position = {300,110},
			},
			
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {

			{	
				widthHeight = {50,16},
				position = {150,276},
			},	
			{	
				widthHeight = {50,16},
				position = {200,276},
			},
			{	
				widthHeight = {50,16},
				position = {250,276},
			},
								{	
				widthHeight = {50,16},
				position = {300,276},
			},	
			{	
				widthHeight = {50,16},
				position = {350,276},
			},
			{	
				widthHeight = {50,16},
				position = {400,276},
			},
			{	
				widthHeight = {50,16},
				position = {450,276},
			},	

		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
			{
				widthHeight = {50,48},
				position ={0,276},
				allowedMovement = 380, --How far left-right the enemy can walk.
				speed = -28 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,276},
				allowedMovement = 280, --How far left-right the enemy can walk.
				speed = -18 --How fast they walk. Start walking left.
			},
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		flags = {
		},
	},

	-------------------------------------------
	--Screen 3
	-------------------------------------------
	{
		--Create the blocks for this screen. Leave blank for none.
		blocks = {
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={50,276},
			},

			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={240,260},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={340,186},
			},

		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
			{	
				widthHeight = {30,42},
				position = {240,200},
			},
			
						{	
				widthHeight = {30,42},
				position = {340,126},
			},
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {
							{	
				widthHeight = {50,16},
				position = {0,276},
			},	
			{	
				widthHeight = {50,16},
				position = {50,276},
			},
			{	
				widthHeight = {50,16},
				position = {100,276},
			},
			{	
				widthHeight = {50,16},
				position = {150,276},
			},	
			{	
				widthHeight = {50,16},
				position = {200,276},
			},
			{	
				widthHeight = {50,16},
				position = {250,276},
			},
								{	
				widthHeight = {50,16},
				position = {300,276},
			},	
			{	
				widthHeight = {50,16},
				position = {350,276},
			},
			{	
				widthHeight = {50,16},
				position = {400,276},
			},
			{	
				widthHeight = {50,16},
				position = {450,276},
			},	
		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		flags = {
		},
	},


	-------------------------------------------
	--Screen 4
	-------------------------------------------
	{
		--Create the blocks for this screen. Leave blank for none.
		blocks = {
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={100,156},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={200,156},
			},
			{
				filename = "images/block_grey_plain.png",
				type = "plain",
				widthHeight = {50,50},
				position ={250,156},
			},
			{
				filename = "images/block_push.png",
				type = "pushable",
				widthHeight = {50,50},
				position ={250,106},
			},
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {

		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {
						{	
				widthHeight = {50,16},
				position = {0,276},
			},	
			{	
				widthHeight = {50,16},
				position = {50,276},
			},
			{	
				widthHeight = {50,16},
				position = {100,276},
			},
			{	
				widthHeight = {50,16},
				position = {150,276},
			},	
			{	
				widthHeight = {50,16},
				position = {200,276},
			},
			{	
				widthHeight = {50,16},
				position = {250,276},
			},
								{	
				widthHeight = {50,16},
				position = {300,276},
			},	
			{	
				widthHeight = {50,16},
				position = {350,276},
			},
		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		flags = {
		},
	},

	-------------------------------------------
	--Screen 5
	--This is going to be the last screen for this
	--level, so all i do is make a tree! (end point)
	-------------------------------------------
	{
		--Create the blocks for this screen. Leave blank for none.
		blocks = {
		},
		--Create the Coins for this screen. Leave blank for none.
		coins = {
			{	
				widthHeight = {30,42},
				position = {50,266},
			},
			{	
				widthHeight = {30,42},
				position = {150,266},
			},
			{	
				widthHeight = {30,42},
				position = {250,266},
			},
		},
		--Create the Spikes for this screen. Leave blank for none.
		spikes = {
		},
		--Create some enemies. Very basic walking. Don't create too close to blocks!
		enemies = {
					{
				widthHeight = {50,48},
				position ={50,276},
				allowedMovement = 400, --How far left-right the enemy can walk.
				speed = -24 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={400,276},
				allowedMovement = 400, --How far left-right the enemy can walk.
				speed = -22 --How fast they walk. Start walking left.
			},
			{
				widthHeight = {50,48},
				position ={300,276},
				allowedMovement = 400, --How far left-right the enemy can walk.
				speed = -23 --How fast they walk. Start walking left.
			},
						{
				widthHeight = {50,48},
				position ={300,276},
				allowedMovement = 400, --How far left-right the enemy can walk.
				speed = -28 --How fast they walk. Start walking left.
			},
									{
				widthHeight = {50,48},
				position ={300,276},
				allowedMovement = 400, --How far left-right the enemy can walk.
				speed = -8 --How fast they walk. Start walking left.
			},
		},
		--Create the level end. (flag) Should only have one of these and in the last screen you make.
		--To edit its appearance and functions you would have to edit the createSection function in the game.lua file.
		flags = {
			{	
				widthHeight = {80,140},
				position = {320,276}, --BottomCenterReferencePoint
			},	
		},
	},
}


--Set up the screen bounds..
--Limits you going too far left or right. The first one should ALWAYS be 0.
--The second value controls how far right in pixels you can travel. 
--480 = 1 screen, 960 = 2 screens, 1440 = 3 screens, 1920 = 4 screens, 2400 = 5 screens.
M.screenBounds = {0,2400}




--Return it all to the game.
return M

