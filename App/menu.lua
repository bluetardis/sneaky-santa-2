-------------------------------------------------------------------------
--Blue Tardis
--Sneaky Santa
--Created by Peter Bishop & Nickolas Kola
--
-- MENU.LUA
-----------------------------------------------------------------
display.setStatusBar( display.HiddenStatusBar )


--Get the HEIGHT & WIDTH of the Device's Screen
_SCREEN = {
		HEIGHT = display.contentHeight,
		WIDTH = display.contentWidth
}

--Get the CENTER POINT of the Device's Screen
_SCREEN.CENTER = {
	x = display.contentCenterX,
	y = display.contentCenterY
}

--globals for us developers to work with 
globals = require("globals")


--Start off by requiring storyboard and creating a scene.
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()


--Variables etc we needs
local _W = display.contentWidth --Width and height parameters
local _H = display.contentHeight 
local tapChannel, tapSound --Sound variables..


--turn music back on
--print( "fade in bg music menu.lua" )
audio.fade( { channel=10, time=2500, volume=1 } )



-- ------------------------------------------------
-- -- *** STORYBOARD SCENE EVENT FUNCTIONS ***
-- ------------------------------------------------
-- -- Called when the scene's view does not exist:
-- -- Create all your display objects here.
-- function scene:createScene( event )
-- 	--print( "Menu: createScene event")
-- 	local screenGroup = self.view
	
-- 	--Load the sounds.
-- 	tapSound = audio.loadSound("sounds/tapsound.wav")

-- 	--Background images first...
-- 	local bg1 = display.newImageRect( "images/mainMenu.jpg", 480,320)
-- 	bg1.x = _W*0.5; bg1.y = _H*0.5
-- 	screenGroup:insert(bg1)

-- 	--Play Game button;
-- 	local function startGame()
-- 		tapChannel = audio.play( tapSound )
-- 		storyboard.gotoScene( "levelSelect", "slideLeft", 400 )
-- 	end
-- 	local playGame = display.newRect(0,0, 120, 60)
-- 	playGame.x = _W*0.5; playGame.y = _H*0.8; playGame.alpha = 0.01
-- 	playGame:addEventListener("tap", startGame)
-- 	screenGroup:insert(playGame)
-- end

--Declaring the Blue Color Gradient Fill for the Background
local redGradientFill = {
	type = "gradient",
	color1 = {
			255/255,
			65/255,
			65/255,
			1
	},
	color2 = {
			255/255,
			65/255,
			65/255,
			1
	},
	direction = "down"
}

--Background Rectangle with Previously Declared Blue Fill
local bg = display.newRect( 0, 0, _SCREEN.WIDTH, _SCREEN.HEIGHT)
bg.x = _SCREEN.CENTER.x
bg.y = _SCREEN.CENTER.y
bg.fill = redGradientFill

--Display the Santa Logo in the Middle
local logo = display.newImage("santalogo.png",_SCREEN.CENTER.x + 7, _SCREEN.CENTER.y)
logo.height = 200;
logo.width = 130;

--Display Go Button / Image
--local gobutton = display.newRect(_SCREEN.CENTER.x * 1.5, _SCREEN.CENTER.y * 1.45, _SCREEN.WIDTH * 0.5, _SCREEN.HEIGHT * 0.20)

--Display Play Button
local startbutton = display.newImage("images/startbutton.png", _SCREEN.CENTER.x * 1.5, _SCREEN.CENTER.y * 1.45)
startbutton.height = 50;
startbutton.width = 250;

--Display Sneaky Santa Text
local textlogo = display.newImage("images/sneakysantatext.png", _SCREEN.CENTER.x * 1.5, _SCREEN.CENTER.y * 0.7)
textlogo.height = 140;
textlogo.width = 240;

----------------------------------
--Logo Transition
transition.from(logo, {
	time = 1000,
	yScale = 0.1,
	xScale = 0.1,
	transition = easing.outBounce,
	})

transition.to(logo,{
		time = 1000,
		alpha = 1,
		transition = easing.outQuad,
	})

transition.to(logo,{
		delay = 2500,
		time = 1000,
		yScale = 1.50,
		xScale = 1.50,
		 y = _SCREEN.CENTER.y * 1.0,
		 x = _SCREEN.CENTER.x + -115,
		 transition = easing.outBounce,
})

--Go Button Transition
transition.from(startbutton, {
		delay = 3000,
		time = 1000,
		x = _SCREEN.CENTER.x + 400,
		transition = easing.outBounce,
		})

--Sneaky Santa Text Transition
transition.from(textlogo, {
		delay = 2500,
		time = 1000,
		x = _SCREEN.CENTER.x + 400,
		transition = easing.outBounce,
		})

--When the Start Button is pressed
tapSound = audio.loadSound("sounds/tapsound.wav")

local function startGame()
tapChannel = audio.play( tapSound )
storyboard.gotoScene( "levelSelect", "slideLeft", 400 )
end

startbutton:addEventListener("tap", startGame)

-- Called immediately after scene has moved onscreen:
-- Start timers/transitions etc.
function scene:enterScene( event )
	--print( "Menu: enterScene event" )
	
	-- Completely remove the previous scene/all scenes.
	-- Handy in this case where we want to keep everything simple.
	storyboard.removeAll()
end

-- Called when scene is about to move offscreen:
-- Cancel Timers/Transitions and Runtime Listeners etc.
function scene:exitScene( event )
	--print( "Menu: exitScene event" )
end

--Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	--print( "Menu: destroying view" )
	audio.dispose( tapSound ); tapSound = nil;
end



-----------------------------------------------
-- Add the story board event listeners
-----------------------------------------------
scene:addEventListener( "createScene", scene )
scene:addEventListener( "enterScene", scene )
scene:addEventListener( "exitScene", scene )
scene:addEventListener( "destroyScene", scene )



--Return the scene to storyboard.
return scene
